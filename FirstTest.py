from selenium.webdriver import Chrome
from selenium.webdriver.support.select import Select
from faker import Faker
import random

fake_data = Faker()

print("Demo Automation"
      "\n A. Register New Parishioner"
      "\n B. New Parishioner")

demotype = input("Plese Select Demo Automation: ")

def driverSetup():

    path = "C:\\Users\\flign\\Downloads\\chromedriver.exe"
    global driver
    driver = Chrome(executable_path=path)

    driver.get("http://devapp.pfpd.com/login")
    driver.maximize_window()

def createParishionerGoodPath():

    driverSetup()

    chapelIndex = random.randint(0, 2)
    driver.find_element_by_name("email").send_keys("admin@gmail.com")
    driver.find_element_by_name("password").send_keys("123456")
    driver.find_element_by_xpath("//button[@type='submit']").click()
    driver.find_element_by_link_text("2019 Attendance").click()
    driver.find_element_by_name("newParticipants").click()
    driver.find_element_by_name("first_name").send_keys(fake_data.first_name())
    driver.find_element_by_name("last_name").send_keys(fake_data.last_name())
    obj = Select(driver.find_element_by_name("chapel"))
    obj.select_by_index(chapelIndex)
    # driver.find_element_by_xpath("//select[@name='chapel']/option[@index='0'").click()
    driver.find_element_by_name("registered").click()
    driver.find_element_by_name("paid").click()
    driver.find_element_by_name("remarks").send_keys(fake_data.word())
    driver.find_element_by_xpath("//button[@name='submitname']").click()

if(demotype=='A' or demotype=='a'):
    print("Demo Automation"
          "\n A. Goood Path"
          "\n B. Bad Path")
    pathType = input("Please Select Path: ")

if(pathType=='A' or pathType=='a'):
    createParishionerGoodPath()